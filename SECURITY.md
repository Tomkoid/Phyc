### Supported Versions

| Version | Supported          |
| ------- | ------------------ |
| v4      | :white_check_mark: |
| v3.1    | :x:                |
| v3      | :x:                |
| v2.2    | :x:                |
| v2.11   | :x:                |
| v2.1    | :x:                |
| v2.0    | :x:                |
| v1.0    | :x:                |