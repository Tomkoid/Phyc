module.exports = {
    owoifytext: function(text) {
        let owoifiedtext = text
        .toLowerCase()
        .replaceAll("r", "w")
        .replaceAll("R", "W")
        .replaceAll("l", "w")
        .replaceAll("L", "w")
        .replaceAll("love", "wuv")
        .replaceAll("mr", "mistuh")
        .replaceAll("dog", "doggo")
        .replaceAll("hewwo", "henwo")
        .replaceAll("friend", "fwend")
        .replaceAll("stop", "stawp")
        .replaceAll("god", "gosh")
        .replaceAll("damn", "darn")
        .replaceAll("heww", "heck")
        .replaceAll("i", "I");
    
        return owoifiedtext;
    }
}
